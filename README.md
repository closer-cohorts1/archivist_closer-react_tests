## List of tests on [wiki page](https://wiki.ucl.ac.uk/display/CTTEAM/Archivist+tests)

- [x] Create/delete an instrument
- [x] Create/delete a question item
- [x] Create/delete a question group
- [x] Create/delete a code list
- [x] Create/delete a response domain
  - [x] text
  - [x] numeric
  - [x] datetime
- [x] Create/delete a sequence
- [x] Create/delete a statement
- [x] Create/delete a condition
  - [x] Add a construct to true branch
  - [x] Add a construct to else branch
- [x] Create/delete a loop
  - [x] Add a construct to a loop


### Preparation

- Modify URL in mylib.py
```
main_url = "https://closer-archivist-us.herokuapp.com/"
```
